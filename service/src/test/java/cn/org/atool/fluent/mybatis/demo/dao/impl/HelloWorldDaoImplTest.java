package cn.org.atool.fluent.mybatis.demo.dao.impl;

import cn.org.atool.fluent.mybatis.demo.Application;
import cn.org.atool.fluent.mybatis.demo.dao.intf.HelloWorldDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = HelloWorldConfig.class)
@SpringBootTest(classes = Application.class)
class HelloWorldDaoImplTest {
    @Autowired
    HelloWorldDao dao;

    @Test
    void test() {
        dao.deleteById(1);
    }
}